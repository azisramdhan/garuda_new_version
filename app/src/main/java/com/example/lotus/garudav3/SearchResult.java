package com.example.lotus.garudav3;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotus.garudav3.adapter.RouteAdapter;
import com.example.lotus.garudav3.model.Route;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.bumptech.glide.*;
import com.example.lotus.garudav3.ui.SplashScreen;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by Lotus on 20/11/2017.
 */

public class SearchResult extends Fragment{

    private List<Route> routeList = new ArrayList<>();
    private ArrayList<String> departureTimeList = new ArrayList<>();
    private ArrayList<String> arrivalTimeList = new ArrayList<>();
    private ArrayList<String> durationTimeList = new ArrayList<>();
    private ArrayList<String> flightIdentification = new ArrayList<>();
    private ArrayList<String> fareList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;

    private RouteAdapter mAdapter;
    Intent intent;
    String arrivalCode;
    String departureDate;
    String arrivalDate;
    String departureCode;
    String availableSeats;
    int totalPax;
    int addedPrice;
    String totalPrice;
    String timeFlight;
    String durationFlight;
    String flightIdentifier;
    String dateFormatApi = "";
    String totalHarga;
    int totalTax = 0;

    Toolbar toolbar;

    private TextView totalP;

//    String totalPaxStr;
    String arrivalStationTerminal;
    String departureStationTerminal;
    String priceReal;

    boolean hasGoodConnection;
    static final String TAG = BookDetailActivity.class.getSimpleName();

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    Handler handler;
    int Seconds, Minutes, MilliSeconds ;
    TextView timer;
    String totalTime;

    private TextView departureTerminal;
    private TextView arrivalTerminal;
    private TextView departurePort;
    private TextView arrival;
    private TextView summary;
    private long dateDeparture;
    private String departureDateStr;
    private boolean selectArrival;
    private boolean selectDeparture;
    private int totalInfact;
    private int totalAdult;
    private int totalChildren;

    LinearLayout lUpAdult;
    LinearLayout lUpChildren;
    LinearLayout lUpInfact;
    LinearLayout lDownAdult;
    LinearLayout lDownChildren;
    LinearLayout lDownInfact;

    Button btUpAdult;
    Button btUpChildren;
    Button btUpInfact;
    Button btDownAdult;
    Button btDownChildren;
    Button btDownInfact;
    private TextView textTotalAdult;
    private TextView textTotalChildren;
    private TextView textTotalInfact;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        totalAdult = 1;
        totalChildren = 0;
        totalInfact = 0;

        type = "Economy";
        selectArrival = false;
        selectDeparture = false;

        departureRequestCode = "SUB";
        arrivalRequestCode = "CGK";

        mAdapter = new RouteAdapter(routeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItem(getActivity(), LinearLayoutManager.VERTICAL, 16));

        Intent i = getActivity().getIntent();
        departureCode = i.getStringExtra("departureCode");
        arrivalCode = i.getStringExtra("arrivalCode");
        totalPax = i.getIntExtra("totalPax",0);
        addedPrice = i.getIntExtra("addedPrice",0);
        departureDate = i.getStringExtra("departureDate");
        arrivalDate = i.getStringExtra("arrivalDate");
        dateFormatApi = i.getStringExtra("dateFromatApi");

        departureStationTerminal = i.getStringExtra("departureStationTerminal");
        arrivalStationTerminal = i.getStringExtra("arrivalStationTerminal");
        departureTerminal.setText("T. "+departureStationTerminal);
        arrivalTerminal.setText("T. "+arrivalStationTerminal);

        availableSeats = i.getStringExtra("availableSeats");
        departureTimeList = i.getStringArrayListExtra("departureTimeList");
        arrivalTimeList = i.getStringArrayListExtra("arrivalTimeList");
        durationTimeList = i.getStringArrayListExtra("durationTimeList");
        flightIdentification = i.getStringArrayListExtra("flightIdentification");
        fareList = i.getStringArrayListExtra("fareList");

        //new SendHttpPost().execute();
        handler = new Handler() ;

        departurePort.setText(departureCode);
        arrival.setText(arrivalCode);
        Log.d("json", "date: " + departureDate);
//        totalPaxStr = departureDate.split(", ")[1].split(" ")[0] + " " +
//                departureDate.split(", ")[1].split(" ")[1].substring(0,3) + ", " + totalPax + " pax";
//        summary.setText(totalPaxStr);

        for (int j = 0; j<departureTimeList.size(); j++){
            Route route = new Route(
                    fareList.get(j),
                    "Garuda",
                    departureTimeList.get(j)+" - "+arrivalTimeList.get(j),
                    availableSeats+" seat(s) available",
                    durationTimeList.get(j),
                    "Direct",
                    flightIdentification.get(j));
            routeList.add(route);
        }

        mAdapter.notifyDataSetChanged();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                totalPrice = routeList.get(position).getPrice();
                timeFlight = routeList.get(position).getTime();
                durationFlight = routeList.get(position).getDuration();
                flightIdentifier = routeList.get(position).getPoints();

                if(hasNetworkConnection()){
                    new SendHttpRequest().execute();
                }else{
                    // eg smartphone is in airplane mode
                    Toast.makeText(getActivity(), "There is no network connection, activate your data cellular or connect through wifi connection", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(getActivity(), "Long press on position :"+position, Toast.LENGTH_LONG).show();
            }
        }));

        // added after changed version
        departurePort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getContext(), SearchDestinationActivity.class);
                String title = "Origin List";
                intent.putExtra("title",title);
                startActivityForResult(intent, REQUEST_CODE_DEPARTURE);
            }
        });
        arrival.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getContext(), SearchDestinationActivity.class);
                String title = "Destination List";
                intent.putExtra("title",title);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
        totalP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // custom dialog
                final Dialog dialog = new Dialog(getActivity());

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.book_detail);
                dialog.setTitle("");
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.BOTTOM;
                window.setDimAmount(0.5f);
                window.setAttributes(wlp);

                textTotalAdult = (TextView)dialog.findViewById(R.id.total_adult);
                textTotalChildren = (TextView)dialog.findViewById(R.id.total_children);
                textTotalInfact = (TextView)dialog.findViewById(R.id.total_infact);

                textTotalAdult.setText(""+totalAdult);
                textTotalChildren.setText(""+totalChildren);
                textTotalInfact.setText(""+totalInfact);

                lUpAdult = (LinearLayout)dialog.findViewById(R.id.up_adult);
                lUpChildren = (LinearLayout)dialog.findViewById(R.id.up_children);
                lUpInfact = (LinearLayout)dialog.findViewById(R.id.up_infact);

                lDownAdult = (LinearLayout)dialog.findViewById(R.id.down_adult);
                lDownChildren = (LinearLayout)dialog.findViewById(R.id.down_children);
                lDownInfact = (LinearLayout)dialog.findViewById(R.id.down_infact);

                btUpAdult = (Button)dialog.findViewById(R.id.bt_up_adult);
                btUpChildren = (Button)dialog.findViewById(R.id.bt_up_children);
                btUpInfact = (Button)dialog.findViewById(R.id.bt_up_infact);

                btDownAdult = (Button)dialog.findViewById(R.id.bt_down_adult);
                btDownChildren = (Button)dialog.findViewById(R.id.bt_down_children);
                btDownInfact = (Button)dialog.findViewById(R.id.bt_down_infact);

                lUpAdult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        upAdult();
                    }
                });
                lUpChildren.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        upChildren();
                    }
                });
                lUpInfact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        upInfact();
                    }
                });
                lDownAdult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downAdult();
                    }
                });
                lDownChildren.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downChildren();
                    }
                });
                lDownInfact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downInfact();
                    }
                });

                btUpAdult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        upAdult();
                    }
                });
                btUpChildren.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        upChildren();
                    }
                });
                btUpInfact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        upInfact();
                    }
                });

                btDownAdult.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downAdult();
                    }
                });
                btDownChildren.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downChildren();
                    }
                });
                btDownInfact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downInfact();
                    }
                });
                dialog.show();
            }
        });
        summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // custom dialog
                final Dialog dialog = new Dialog(getActivity());

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.date_picker);
                dialog.setTitle("");
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                WindowManager.LayoutParams wlp = window.getAttributes();

                wlp.gravity = Gravity.BOTTOM;
                window.setDimAmount(0.5f);
                window.setAttributes(wlp);

                CalendarView datePicker = (CalendarView) dialog.findViewById(R.id.date_picker);
                datePicker.setMinDate(System.currentTimeMillis()+86400000);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                selectedDate=calendar.get(Calendar.DAY_OF_MONTH);
                selectedMonth=calendar.get(Calendar.MONTH);
                selectedYear=calendar.get(Calendar.YEAR);
                datePicker.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                        dateFormatApi = "";
                        String dayForApi = dayOfMonth < 10 ? dateFormatApi + "0" + dayOfMonth : dateFormatApi + ""+dayOfMonth;
                        String monthForApi = month < 10 ? dateFormatApi + "0" + (month+1) : dateFormatApi + ""+ (month+1);
                        String yearForApi = dateFormatApi + String.valueOf(year).substring(2,4);

                        dateFormatApi = dayForApi + monthForApi + yearForApi;
                        Log.e("dateFormatApi", ""+ dateFormatApi);

                        if(selectedDate ==dayOfMonth && selectedMonth==month && selectedYear==year) {
                            dialog.dismiss();
                        }else {
                            if(selectedDate !=dayOfMonth){
                                dialog.dismiss();
                            }else {
                                if(selectedMonth !=month){
                                    dialog.dismiss();
                                }
                            }
                        }
                        selectedDate=dayOfMonth;
                        selectedMonth=month;
                        selectedYear=year;

                        String input = (selectedMonth+1) + " " + selectedDate + " " + selectedYear;
//                        Log.i("input",input);

                        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
//                        Date date = new Date(selectedYear, selectedMonth, selectedDate);
                        Date date = null;
                        try {
                            date = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH).parse(input);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dateDeparture = date.getTime();
                        String dayOfWeek = simpledateformat.format(date);
                        String monthStr = getMonth(month+1);
                        departureDateStr = "" + dayOfWeek + ", " + selectedDate + " " + monthStr;

                        summary.setText(departureDateStr.split(", ")[1]);
                        Toast.makeText(getActivity(), "" + dayOfWeek + ", " + selectedDate + " ", Toast.LENGTH_SHORT).show();

                        new SendHttpRequestFirst().execute();
                    }

                });
                dialog.show();
            }
        });


    }


    public void upAdult(){
        totalAdult++;
        updateNumber();
    }
    public void downAdult(){
        if(totalAdult > 0){
            totalAdult--;
        }
        updateNumber();
    }
    public void upChildren(){
        totalChildren++;
        updateNumber();
    }
    public void downChildren(){
        if(totalChildren > 0){
            totalChildren--;
        }
        updateNumber();
    }
    public void upInfact(){
        totalInfact++;
        updateNumber();
    }
    public void downInfact(){
        if(totalInfact > 0){
            totalInfact--;
        }
        updateNumber();
    }

    String type;

    void updateNumber(){

        totalPax = totalAdult + totalChildren + totalInfact;

        String adult = "" + totalAdult;
        String children = "" + totalChildren;
        String infact = "" + totalInfact;
        textTotalAdult.setText(adult);
        textTotalChildren.setText(children);
        textTotalInfact.setText(infact);

        totalP.setText(adult + " adult, " + children + " children, " + infact + " infact, " + type + " Class");

    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    int REQUEST_CODE = 99;
    int REQUEST_CODE_DEPARTURE = 11;
    private int selectedDate,selectedMonth,selectedYear;
    String departureRequestCode;
    String arrivalRequestCode;


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            if(requestCode == REQUEST_CODE){
                // Jika kembali dengan menekan list item
                String airport = data.getStringExtra("airport");
                arrivalRequestCode = airport.substring(0, airport.indexOf(" "));
                selectArrival = true;
            }else if(requestCode == REQUEST_CODE_DEPARTURE){
                // Jika kembali dengan menekan list item
                String airport = data.getStringExtra("airport");
                departureRequestCode = airport.substring(0, airport.indexOf(" "));
                selectDeparture = true;
            }
            new SendHttpRequestFirst().execute();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_search, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_route);
        departureTerminal = (TextView)view.findViewById(R.id.departure);
        arrivalTerminal = (TextView)view.findViewById(R.id.arrival);
        departurePort = (TextView)view.findViewById(R.id.departure_port);
        arrival = (TextView)view.findViewById(R.id.arrival_port);
        summary = (TextView)view.findViewById(R.id.summary_book);
        totalP = (TextView)view.findViewById(R.id.totalPax);

        return view;
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }


    /**
     * Background thread with all of error handling code start from here until the end of line
     * */

    @SuppressLint("StaticFieldLeak")
    private class SendHttpRequest extends AsyncTask<Void, Void, Void>{

        //static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/PriceWithoutPNR";
        static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/PriceWithoutPNRWithCache";

        // before background thread created
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hasGoodConnection = true;
            pDialog = new ProgressDialog(getActivity(),R.style.full_screen_dialog){
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.fill_dialog);
                    getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);

                    /*ImageView imageView = (ImageView) getWindow().findViewById(R.id.progressBar1);
                    Glide.with(SearchResult.this).load(R.raw.loading9).into(imageView);*/

                    timer = (TextView) getWindow().findViewById(R.id.timer);

                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);

                }
            };

            pDialog.setCancelable(false);
            pDialog.show();
        }

        // create background thread
        @Override
        protected Void doInBackground(Void... voids) {

            // construct URL from input parameter for url query
            Uri builtUrl = Uri.parse(REQUEST_URL).buildUpon()
                    .build();

            // create URL object from given string url
            URL url = createUrl(builtUrl.toString());
            Log.i("URL",builtUrl.toString());
            String jsonResponse = "";
            try {
                jsonResponse = makeHttpRequest(url,"0");
                Log.i("JSON Response",jsonResponse);
            } catch (IOException e) {
                Log.e(TAG," Failed to create http request");
                hasGoodConnection = false;
                e.printStackTrace();
            }
            extractResponse(jsonResponse);
            return null;
        }

        // background thread finish
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
                handler.removeCallbacks(runnable);
            }

            if(hasGoodConnection){
                intent = new Intent(getActivity(), FillDetails.class);
                intent.putExtra("departureCode",departureCode);
                intent.putExtra("arrivalCode",arrivalCode);
                intent.putExtra("totalPax",totalPax);
                intent.putExtra("addedPrice",addedPrice);
                intent.putExtra("departureDate",departureDate);
                intent.putExtra("arrivalDate",arrivalDate);
                intent.putExtra("price",totalPrice);
                intent.putExtra("timeFlight", timeFlight);
                intent.putExtra("flightIdentification",flightIdentifier);
                intent.putExtra("totalHarga",totalHarga);
                intent.putExtra("durationFlight",durationFlight);
                intent.putExtra("totalTax",totalTax);
//                intent.putExtra("totalPaxStr", totalPaxStr);
                intent.putExtra("totalTime",totalTime);

                intent.putExtra("departureStationTerminal",departureStationTerminal);
                intent.putExtra("arrivalStationTerminal",arrivalStationTerminal);
                startActivity(intent);
            }else{
                Toast.makeText(getActivity(), "Check your network connection and try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // create URL object from url String
    public static URL createUrl(String urlStr){
        // return null if URL not created
        URL url = null;
        try {
            // create URL object
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            // create URL not allowed
            Log.e(TAG, "Problem building the URL ", e);
            e.printStackTrace();
        }
        return url;
    }

    // perform a network request
    public String makeHttpRequest(URL url, String type) throws IOException {
        String jsonResponse="";

        if(url == null){
            Log.e(TAG,"There is no valid string url");
            hasGoodConnection = false;
            return null;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            // url.openConnection return HttpURLConnection that extend URLConnection
            urlConnection = (HttpURLConnection)url.openConnection();
            // http request method
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            // set timeout request
            urlConnection.setConnectTimeout(10000);/* milliseconds */
            urlConnection.setReadTimeout(10000);/* milliseconds */
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            JSONObject jsonRequestBody;
            if(type == "0"){
                // create jsonObject for HTTP request body
                jsonRequestBody = createJsonObject();
            }else{
                // create jsonObject for HTTP request body
                jsonRequestBody = createJsonObjectFirst();
            }

            if(jsonRequestBody != null){
                // send the post body
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                bufferedWriter.write(jsonRequestBody.toString());
                bufferedWriter.flush();

                // log response message and response code
                Log.i("Response Code ", String.valueOf(urlConnection.getResponseCode()));
                Log.i("Response Message ", urlConnection.getResponseMessage());

                departureTimeList.clear();
                arrivalTimeList.clear();
                durationTimeList.clear();

            }else{
                Log.e(TAG, "There is no valid JSON object");
                hasGoodConnection = false;
            }

            // if the request was succesful (response code 200)
            // then read the input stream and parse the response
            if(urlConnection.getResponseCode() == 200){
                // get the input stream from given URL
                inputStream = urlConnection.getInputStream();
                // read inputStream and return a String
                jsonResponse = readFromInputStream(inputStream);
            }
            // else, there is no input stream contain information that we want to read
            else{
                Log.e(TAG,"Error response code: "+urlConnection.getResponseCode());
                hasGoodConnection = false;
            }
        }catch (Exception e){
            hasGoodConnection = false;
            // eg there is low cellular data on smartphone
            Log.e(TAG,"Problem retrieving JSON response.",e);
        }finally {
            // cleaning the resource

            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(inputStream != null){
                // function must handle java.io.IOException here
                inputStream.close();
            }
        }

        return jsonResponse;
    }

    // read input stream from API web services and return it to human readable format
    private static String readFromInputStream(InputStream inputStream) throws IOException {
        // to store information from input stream
        StringBuilder output = new StringBuilder();
        // read input stream
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        // represent input stream to human readable text format
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();
        while (line != null){
            output.append(line);
            line = bufferedReader.readLine();
        }
        // return information
        return  output.toString();
    }

    // create JSON object for HTTP POST request body
    public JSONObject createJsonObject(){

        try{
            JSONObject ptcGroupObject = new JSONObject();
            ptcGroupObject.put("value_qualifier", "ADT");
            ptcGroupObject.put("qualifier", "0");

            JSONArray ptcGroupArray = new JSONArray();
            ptcGroupArray.put(ptcGroupObject);

            JSONObject segmentControlDetailsObject = new JSONObject();
            segmentControlDetailsObject.put("number_of_units", 2);
            segmentControlDetailsObject.put("quantity", 1);

            JSONArray segmentControlDetailArray = new JSONArray();
            segmentControlDetailArray.put(segmentControlDetailsObject);

            JSONObject travellerDetailsObject = new JSONObject();
            travellerDetailsObject.put("measurement_value", 1);

            JSONObject travellerDetailsObjectDua = new JSONObject();
            travellerDetailsObjectDua.put("measurement_value", 2);

            JSONArray travellerDetailsArray = new JSONArray();
            travellerDetailsArray.put(travellerDetailsObject);
            travellerDetailsArray.put(travellerDetailsObjectDua);

            JSONObject passengerGroupObject = new JSONObject();
            passengerGroupObject.put("ptc_groups", ptcGroupArray);
            passengerGroupObject.put("segment_control_details", segmentControlDetailArray);
            passengerGroupObject.put("traveller_details", travellerDetailsArray);

            JSONArray passengerGroupArray = new JSONArray();
            passengerGroupArray.put(passengerGroupObject);

            JSONObject pricingOptionsGroup = new JSONObject();
            pricingOptionsGroup.put("marketing_company", "GA");

            JSONArray pricingOptionsGroupArray = new JSONArray();
            pricingOptionsGroupArray.put(pricingOptionsGroup);

            JSONArray flightIndicatorArray = new JSONArray();
            flightIndicatorArray.put("1");

            JSONObject segmentGroupObject = new JSONObject();
            segmentGroupObject.put("board_point_true_location_id", departureCode);
            segmentGroupObject.put("off_point_true_location_id", arrivalCode);
            segmentGroupObject.put("flight_marketing_company", "GA");
            segmentGroupObject.put("flight_number", "75");
            segmentGroupObject.put("booking_class", "Y");
            segmentGroupObject.put("flight_indicators", flightIndicatorArray);
            segmentGroupObject.put("item_number", 1);
            segmentGroupObject.put("departure_date", dateFormatApi);

            JSONArray segmentGroupArray = new JSONArray();
            segmentGroupArray.put(segmentGroupObject);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("message_function", "741");
            jsonObject.put("responsible_agency", "1A");
            jsonObject.put("passengers_groups", passengerGroupArray);
            jsonObject.put("pricing_options_groups", pricingOptionsGroupArray);
            jsonObject.put("destination", arrivalCode);
            jsonObject.put("origin", departureCode);
            jsonObject.put("segment_groups", segmentGroupArray);
            jsonObject.put("business_function", "1");

            Log.i("JSON object", jsonObject.toString());
            return jsonObject;
        }catch (JSONException e){
            Log.e(TAG,"Problem creating JSON object.",e);
            hasGoodConnection = false;
            return null;
        }
    }

    // parsing JSON response and initialize some variable from it
    public void extractResponse(String jsonResponse) {

        if(TextUtils.isEmpty(jsonResponse)){
            Log.e(TAG,"There is no valid json response");
            hasGoodConnection = false;
            return;
        }

        // If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // extract response data and initiate with variable that have been made
            JSONObject jsonObj = new JSONObject(jsonResponse);

            JSONArray pricingGroupLevelGroups = jsonObj.getJSONArray("pricingGroupLevelGroups");
            JSONObject pricingGroupLevelGroupsObject = pricingGroupLevelGroups.getJSONObject(0);

            totalHarga = pricingGroupLevelGroupsObject.getString("fareAmount");

            JSONArray taxDetailsArray = pricingGroupLevelGroupsObject.getJSONArray("taxDetails");

            for(int i = 0; i<taxDetailsArray.length(); i++){
                JSONObject taxDetailsObject = taxDetailsArray.getJSONObject(i);
                totalTax = totalTax + Integer.parseInt(taxDetailsObject.getString("taxDetailRate"));
            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(TAG, "Problem parsing JSON response", e);
            hasGoodConnection = false;
        }
    }

    // to check internet connection status on device eg smartphone
    boolean hasNetworkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED ) {
                // notify user you are online
                return true;
            }
            else if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {
                // notify user you are not online
                return false;
            }
        }
        return false;
    }

    public Runnable runnable = new Runnable() {
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);

            totalTime = "" + String.format("%01d", Seconds) + "." + String.format("%03d", MilliSeconds);
            timer.setText(totalTime);

            handler.postDelayed(this, 0);
        }

    };


    /**
     * Background thread with all of error handling code start from here until the end of line
     * */

    @SuppressLint("StaticFieldLeak")
    private class SendHttpRequestFirst extends AsyncTask<Void, Void, Void> {

        static final String REQUEST_URL = "https://altea-proxy.mgmt.apigateway.us/altea/availabilityFare";

        // before background thread created
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hasGoodConnection = true;
            routeList.clear();
            departureTimeList.clear();
            arrivalTimeList.clear();
            durationTimeList.clear();
            flightIdentification.clear();
            fareList.clear();
            mAdapter.notifyDataSetChanged();
        }

        // create background thread
        @Override
        protected Void doInBackground(Void... voids) {

            // construct URL from input parameter for url query
            Uri builtUrl = Uri.parse(REQUEST_URL).buildUpon()
                    .build();

            // create URL object from given string url
            URL url = createUrl(builtUrl.toString());
            Log.i("URL",builtUrl.toString());
            String jsonResponse = "";
            try {
                jsonResponse = makeHttpRequest(url,"1");
                Log.i("JSON Response",jsonResponse);
            } catch (IOException e) {
                Log.e(TAG," Failed to create http request");
                hasGoodConnection = false;
                e.printStackTrace();
            }
            extractResponseFirst(jsonResponse);
            return null;
        }

        // background thread finish
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if(hasGoodConnection){
                if(selectArrival){
                    arrivalTerminal.setText("T. "+arrivalStationTerminal);
                    arrivalCode=arrivalRequestCode;
                    arrival.setText(arrivalCode);
                    selectArrival=false;
                }
                if(selectDeparture){
                    departureTerminal.setText("T. "+departureStationTerminal);
                    departureCode=departureRequestCode;
                    departurePort.setText(departureCode);
                    selectDeparture=false;
                }
                if(departureTimeList.size()==0){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setMessage("There is no route available");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    alertDialog.show();
                }
                for (int j = 0; j<departureTimeList.size(); j++){
                    Route route = new Route(
                            fareList.get(j),
                            "Garuda",
                            departureTimeList.get(j)+" - "+arrivalTimeList.get(j),
                            availableSeats+" seat(s) available",
                            durationTimeList.get(j),
                            "Direct",
                            flightIdentification.get(j));
                    routeList.add(route);
                }
//
                mAdapter.notifyDataSetChanged();
//                Toast.makeText(getActivity(), "Succes, update route departureCode " + departureCode + " arrivalCode " + arrivalCode +" ", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Check your network connection and try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // create JSON object for HTTP POST request body
    public JSONObject createJsonObjectFirst(){

        try{
            JSONObject dateObject = new JSONObject();
            dateObject.put("departure_date", dateFormatApi);

            JSONArray dateArray = new JSONArray();
            dateArray.put(dateObject);

            JSONObject requestObject = new JSONObject();
            requestObject.put("availability_details", dateArray);
            requestObject.put("departure_city", departureRequestCode);
            requestObject.put("arrival_city", arrivalRequestCode);
            requestObject.put("type_of_request", "AN");

            JSONArray requestArray = new JSONArray();
            requestArray.put(requestObject);

            JSONObject segmentControlObject = new JSONObject();
            segmentControlObject.put("number_of_units", 1);
            segmentControlObject.put("quantity", 1);

            JSONArray segmentControlArray = new JSONArray();
            segmentControlArray.put(segmentControlObject);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("request_sections", requestArray);
            jsonObject.put("action_code", "44");
            jsonObject.put("business_function", "1");
            jsonObject.put("segment_control_details", segmentControlArray);

            Log.i("JSON object", jsonObject.toString());
            return jsonObject;
        }catch (JSONException e){
            Log.e(TAG,"Problem creating JSON object.",e);
            hasGoodConnection = false;
            return null;
        }
    }

    // parsing JSON response and initialize some variable from it
    public void extractResponseFirst(String jsonResponse) {

        if(TextUtils.isEmpty(jsonResponse)){
            Log.e(TAG,"There is no valid json response");
            hasGoodConnection = false;
            return;
        }

        // If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // extract response data and initiate with variable that have been made
            JSONObject root = new JSONObject(jsonResponse);
            JSONArray singleCityPairInfos = root.getJSONArray("singleCityPairInfos");

            for(int i = 0; i < singleCityPairInfos.length();i++){
                JSONObject singleCityPairInfo = singleCityPairInfos.getJSONObject(i);
                JSONArray flightInfos;
                try {
                    flightInfos = singleCityPairInfo.getJSONArray("flightInfos");
                    for(int j = 0; j < flightInfos.length(); j++)
                    {
                        boolean populateToListView = false;
                        JSONObject flightInfo = flightInfos.getJSONObject(j);
                        String departureTime = flightInfo.getString("departureTime");
                        String arrivalTime = flightInfo.getString("arrivalTime");
                        String durationTime = "0110";

                        try{
                            durationTime = flightInfo.getString("legDuration");
                        }catch (JSONException e){
                            Log.e(TAG, "Problem fetch legDuratin", e);
                        }
                        String identifier = flightInfo.getString("identifier");
                        String flightIdentificationNumber = flightInfo.getString("flightIdentificationNumber");
                        JSONArray productIndicators = flightInfo.getJSONArray("productIndicators");
                        for(int k=0;k<productIndicators.length();k++){
                            String code = productIndicators.getString(k);
                            Log.i("Code", code);
                            if(Objects.equals(code, "D")){
                                populateToListView = true;
                            }
                        }

                        String departureStationTerminal = "3";
                        String arrivalStationTerminal = "2";
                        String fareAmount = flightInfo.getString("fareAmount");
                        //Log.d(TAG, "harga: " + "Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(fareAmount)));

                        try{
                            departureStationTerminal = flightInfo.getString("departureStationTerminal");
                        }catch (JSONException e){
                            Log.e(TAG, "Problem fetch departureStationTerminal", e);
                        }

                        try{
                            arrivalStationTerminal = flightInfo.getString("arrivalStationTerminal");
                        }catch (JSONException e){
                            Log.e(TAG, "Problem fetch arrivalStationTerminal", e);
                        }

                        if(selectDeparture)
                            if(!Objects.equals(departureStationTerminal, ""))
                                this.departureStationTerminal = departureStationTerminal;

                        if(selectArrival)
                            if(!Objects.equals(arrivalStationTerminal, ""))
                                this.arrivalStationTerminal = arrivalStationTerminal;

                        if(populateToListView){
                            departureTimeList.add(departureTime.substring(0,2)+"."+departureTime.substring(2, 4));
                            arrivalTimeList.add(arrivalTime.substring(0,2)+"."+arrivalTime.substring(2, 4));
                            durationTimeList.add(durationTime.substring(0,2).replace("0","")+"h "+durationTime.substring(2, 4)+"m");
                            flightIdentification.add(identifier+flightIdentificationNumber);
                            fareList.add("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(fareAmount)));
                        }

                        JSONArray infoClasses = flightInfo.getJSONArray("infoOnClasses");
                        Log.d("TAG", ""+infoClasses.length());

                        for(int k = 0; k<infoClasses.length(); k++) {
                            JSONObject infoClassesObject = infoClasses.getJSONObject(k);
                            String servicesClass = infoClassesObject.getString("serviceClass");
                            if(servicesClass.contains("Y")){
                                availableSeats = infoClassesObject.getString("availabilityStatus");
                            }
                        }
                    }
                }catch (JSONException e){
                    Log.e(TAG, "Null Value of Flight Infos", e);
                }

            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(TAG, "Problem parsing JSON response", e);
            hasGoodConnection = false;
        }
    }

}