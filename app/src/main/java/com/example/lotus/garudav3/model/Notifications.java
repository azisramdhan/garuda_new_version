package com.example.lotus.garudav3.model;

/**
 * Created by Lotus on 16/01/2018.
 */

public class Notifications {
    private String title, subtitle;

    public Notifications() {
    }

    public Notifications(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subName) {
        this.subtitle = subName;
    }
}