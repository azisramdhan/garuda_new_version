package com.example.lotus.garudav3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Lotus on 20/11/2017.
 */

public class FillDetailsRound extends AppCompatActivity {

    Toolbar toolbar;
    Button continueBook;
    Intent intent;
    String arrivalCode;
    String departureDate;
    String arrivalDate;

    String departureCode;
    String timeFlight;
    int totalPax;
    int addedPrice;
    int priceInt = 0;
    int priceIntDua = 0;
    int totalTax = 0;
    int totalTaxDua = 0;

    CheckBox travelInsurance;

    TextView priceTotal;
    TextView priceTotalDua;
    TextView time;
    TextView timeDua;
    TextView duration;
    TextView durationDua;
    TextView flightIdentifier;
    TextView flightIdentifierDua;
    TextView totalSatu;
    TextView totalDua;
    TextView priceTotalBawah;
    TextView priceTotalDuaBawah;
    TextView timerTv;

    String totalTime;
    String arrivalStationTerminal;
    String departureStationTerminal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_detail_round);

        continueBook = (Button) findViewById(R.id.continue_book);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        travelInsurance = (CheckBox) findViewById(R.id.travel_insurance);

        Intent i = getIntent();
        departureCode = i.getStringExtra("departureCode");
        arrivalCode = i.getStringExtra("arrivalCode");
        totalPax = i.getIntExtra("totalPax",0);
        departureDate = i.getStringExtra("departureDate");
        arrivalDate = i.getStringExtra("arrivalDate");
        totalTime = i.getStringExtra("totalTime");

        final String price = i.getStringExtra("price");
        final String timeFlight = i.getStringExtra("timeFlight");
        final String durationFlight = i.getStringExtra("durationFlight");
        final String flightIdentification = i.getStringExtra("flightIdentification");
        final String priceDua = i.getStringExtra("priceDua");
        final String timeFlightDua = i.getStringExtra("timeFlightDua");
        final String durationFlightDua = i.getStringExtra("durationFlightDua");
        final String flightIdentificationDua = i.getStringExtra("flightIdentificationDua");

        departureStationTerminal = i.getStringExtra("departureStationTerminal");
        arrivalStationTerminal = i.getStringExtra("arrivalStationTerminal");
        final TextView departureTerminal = (TextView)findViewById(R.id.departure);
        TextView arrivalTerminal = (TextView)findViewById(R.id.arrival);
        departureTerminal.setText("T. "+departureStationTerminal);
        arrivalTerminal.setText("T. "+arrivalStationTerminal);

        final String totalHarga = i.getStringExtra("totalHarga");
        totalTax = i.getIntExtra("totalTax", 0);
        final String totalHargaDua = i.getStringExtra("totalHargaDua");
        totalTaxDua = i.getIntExtra("totalTaxDua", 0);

        //priceInt = Integer.parseInt(price);
        Log.d("TAG", totalTax+"");

        TextView arrival = (TextView)findViewById(R.id.arrival_port);
        TextView summary = (TextView)findViewById(R.id.summary_book);
        TextView summaryBawah = (TextView)findViewById(R.id.summary_book_bawah);
        TextView priceStr = (TextView)findViewById(R.id.price);
        TextView priceStrDua = (TextView)findViewById(R.id.price_dua);
        timerTv = (TextView) findViewById(R.id.timerTv);

        time = (TextView) findViewById(R.id.time);
        duration = (TextView) findViewById(R.id.duration);
        priceTotal = (TextView)findViewById(R.id.price_total);
        flightIdentifier = (TextView)findViewById(R.id.point);
        timeDua = (TextView) findViewById(R.id.time_dua);
        durationDua = (TextView) findViewById(R.id.duration_dua);
        priceTotalDua = (TextView)findViewById(R.id.price_dua);
        flightIdentifierDua = (TextView)findViewById(R.id.point_dua);
        totalSatu = (TextView)findViewById(R.id.total_satu);
        totalDua = (TextView)findViewById(R.id.total_dua);
        priceTotalBawah = (TextView)findViewById(R.id.price_total);
        priceTotalDuaBawah = (TextView)findViewById(R.id.price_total_dua);
        TextView priceReal = (TextView)findViewById(R.id.price_real);
        TextView priceRealDuaBawah = (TextView)findViewById(R.id.price_real_bawah);

        timerTv.setText("Last processed time: " + totalTime + "s");
        arrival.setText(arrivalCode);
        final String totalPaxStr = departureDate.split(", ")[1].split(" ")[0] + " " +
                departureDate.split(", ")[1].split(" ")[1].substring(0,3) + ", " + totalPax + " pax";
        final String totalPaxStrBawah = arrivalDate.split(", ")[1].split(" ")[0] + " " +
                departureDate.split(", ")[1].split(" ")[1].substring(0,3) + ", " + totalPax + " pax";
        summary.setText(totalPaxStr);
        summaryBawah.setText(totalPaxStrBawah);
        priceStr.setText(price);
        priceStrDua.setText(priceDua);

        time.setText(timeFlight);
        duration.setText(durationFlight);
        flightIdentifier.setText(flightIdentification);
        priceReal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(totalHarga)*totalPax));
        priceTotal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(totalHarga)*totalPax));

        timeDua.setText(timeFlightDua);
        duration.setText(durationFlightDua);
        flightIdentifierDua.setText(flightIdentificationDua);
        priceTotalDuaBawah.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(totalHargaDua)*totalPax));
        priceRealDuaBawah.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(Integer.valueOf(totalHargaDua)*totalPax));

        totalSatu.setText(departureCode + " - " + arrivalCode);
        totalDua.setText(arrivalCode + " - " + departureCode);

        priceInt = Integer.parseInt(totalHarga);
        priceIntDua = Integer.parseInt(totalHargaDua);

        travelInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    priceInt += 26000;
                    priceTotal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(priceInt*totalPax));
                }
                else {
                    priceInt -= 26000;
                    priceTotal.setText("Rp " + NumberFormat.getIntegerInstance(Locale.GERMAN).format(priceInt*totalPax));
                }
            }
        });

        TextView departurePort = (TextView)findViewById(R.id.departure_port);
        departurePort.setText(departureCode);
        TextView bagagge = (TextView)findViewById(R.id.bagagge);
        String bagaggeStr = "CGK - " + arrivalCode;
        bagagge.setText(bagaggeStr);

        continueBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String priceFix = priceTotal.getText().toString();

                intent = new Intent(FillDetailsRound.this, ReviewBooking.class);
                intent.putExtra("departureCode", departureCode);
                intent.putExtra("arrivalCode", arrivalCode);
                intent.putExtra("totalPax", totalPax);
                intent.putExtra("totalPaxStr", totalPaxStr);
                intent.putExtra("totalPaxStrBawah", totalPaxStrBawah);
                intent.putExtra("departureDate", departureDate);
                intent.putExtra("arrivalDate", arrivalDate);
                intent.putExtra("price", price);
                intent.putExtra("priceDua", priceDua);
                intent.putExtra("priceInt", String.valueOf(priceInt));
                intent.putExtra("priceIntDua", String.valueOf(priceIntDua));
                intent.putExtra("totalTax",totalTax);
                intent.putExtra("totalTaxDua",totalTaxDua);

                intent.putExtra("departureStationTerminal",departureStationTerminal);
                intent.putExtra("arrivalStationTerminal",arrivalStationTerminal);
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
