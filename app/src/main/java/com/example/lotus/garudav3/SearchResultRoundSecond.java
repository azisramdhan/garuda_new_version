package com.example.lotus.garudav3;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.lotus.garudav3.adapter.RouteAdapter;
import com.example.lotus.garudav3.model.Route;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lotus on 20/11/2017.
 */

public class SearchResultRoundSecond extends AppCompatActivity{

    private List<Route> routeList = new ArrayList<>();
    private ArrayList<String> departureTimeList = new ArrayList<>();
    private ArrayList<String> arrivalTimeList = new ArrayList<>();
    private ArrayList<String> durationTimeList = new ArrayList<>();
    private ArrayList<String> flightIdentification = new ArrayList<>();
    private ArrayList<String> fareList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProgressDialog pDialog;

    private RouteAdapter mAdapter;
    Intent intent;
    String arrivalCode;
    String departureDate;
    String arrivalDate;
    String departureCode;
    String availableSeats;
    int totalPax;
    int addedPrice;

    String totalPrice;
    String totalPriceAwal;
    String timeFlight;
    String timeFlightAwal;
    String durationFlight;
    String durationFlightAwal;
    String flightIdentifier;
    String flightIdentifierAwal;
    String returnDate;
    String priceReal;

    String dateFormatApi = "";
    String dateFormatApiDua = "";
    String totalHarga;
    String totalHargaDua;
    int totalTax = 0;
    int totalTaxDua = 0;
    boolean hasGoodConnection;
    static final String TAG = BookDetailActivity.class.getSimpleName();

    Toolbar toolbar;
    HttpURLConnection conn;

    String arrivalStationTerminal;
    String departureStationTerminal;
    //private static String url = "https://api.androidhive.info/contacts/";
    private static String url = "https://garuda.client.nextflow.tech/garudatest/v1.0/Availability";

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    Handler handler;
    int Seconds, Minutes, MilliSeconds ;
    TextView timer;
    String totalTime;
    TextView timerTv;
    String timerTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_route);

        mAdapter = new RouteAdapter(routeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItem(this, LinearLayoutManager.VERTICAL, 16));

        Intent i = getIntent();
        departureCode = i.getStringExtra("departureCode");
        arrivalCode = i.getStringExtra("arrivalCode");
        totalPax = i.getIntExtra("totalPax",0);
        addedPrice = i.getIntExtra("addedPrice",0);
        departureDate = i.getStringExtra("departureDate");
        arrivalDate = i.getStringExtra("arrivalDate");
        returnDate = i.getStringExtra("dateReturn");
        dateFormatApi = i.getStringExtra("dateFormatApi");
        dateFormatApiDua = i.getStringExtra("dateFormatApiDua");
        timerTotal = i.getStringExtra("totalTime");

        totalPriceAwal = i.getStringExtra("price");
        timeFlightAwal = i.getStringExtra("timeFlight");
        durationFlightAwal = i.getStringExtra("durationFlight");
        flightIdentifierAwal = i.getStringExtra("flightIdentifier");

        availableSeats = i.getStringExtra("availableSeats");
        departureTimeList = i.getStringArrayListExtra("departureTimeList");
        arrivalTimeList = i.getStringArrayListExtra("arrivalTimeList");
        durationTimeList = i.getStringArrayListExtra("durationTimeList");
        flightIdentification = i.getStringArrayListExtra("flightIdentification");
        fareList = i.getStringArrayListExtra("fareList");

        departureStationTerminal = i.getStringExtra("departureStationTerminal");
        arrivalStationTerminal = i.getStringExtra("arrivalStationTerminal");
        TextView departureTerminal = (TextView)findViewById(R.id.departure);
        TextView arrivalTerminal = (TextView)findViewById(R.id.arrival);
        departureTerminal.setText("T. "+arrivalStationTerminal);
        arrivalTerminal.setText("T. "+departureStationTerminal);
        timerTv = (TextView) findViewById(R.id.timerTv);

        //new SendHttpPost().execute();
        handler = new Handler() ;

        timerTv.setText("Last processed time: " + timerTotal + "s");
        TextView departurePort = (TextView)findViewById(R.id.departure_port);
        departurePort.setText(arrivalCode);
        TextView arrival = (TextView)findViewById(R.id.arrival_port);
        arrival.setText(departureCode);
        TextView summary = (TextView)findViewById(R.id.summary_book);
        String totalPaxStr = departureDate.split(", ")[1].split(" ")[0] + " " +
                departureDate.split(", ")[1].split(" ")[1].substring(0,3) + ", " + totalPax + " pax";
        summary.setText(totalPaxStr);

        if(departureTimeList.size()==0){
            AlertDialog alertDialog = new AlertDialog.Builder(SearchResultRoundSecond.this).create();
            alertDialog.setMessage("There is no route available");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alertDialog.show();
        }

        /*switch(arrivalCode){
            case "SUB":
                priceReal = "Rp 1.372.000";
                break;
            case "DPS":
                priceReal = "Rp 1.651.000";
                break;
            case "LOP":
                priceReal = "Rp 1.636.000";
                break;
            case "JOG":
                priceReal = "Rp 998.000";
                break;
            case "KNO":
                priceReal = "Rp 2.108.000";
                break;
            case "DJJ":
                priceReal = "Rp 5.473.000";
                break;
            default:
                priceReal = "Rp 1.455.000";
                break;
        }*/

        for (int j = 0; j<departureTimeList.size(); j++){
            Route route = new Route(
                    fareList.get(j),
                    "Garuda",
                    departureTimeList.get(j)+" - "+arrivalTimeList.get(j),
                    availableSeats+" seat(s) available",
                    durationTimeList.get(j),
                    "Direct",
                    flightIdentification.get(j));
            routeList.add(route);
        }

        mAdapter.notifyDataSetChanged();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                totalPrice = routeList.get(position).getPrice();
                timeFlight = routeList.get(position).getTime();
                durationFlight = routeList.get(position).getDuration();
                flightIdentifier = routeList.get(position).getPoints();

                if(hasNetworkConnection()){
                    new SendHttpRequest().execute();
                }else{
                    // eg smartphone is in airplane mode
                    Toast.makeText(SearchResultRoundSecond.this, "There is no network connection, activate your data cellular or connect through wifi connection", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(SearchResultRoundSecond.this, "Long press on position :"+position, Toast.LENGTH_LONG).show();
            }
        }));
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }*/
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Background thread with all of error handling code start from here until the end of line
     * */

    @SuppressLint("StaticFieldLeak")
    private class SendHttpRequest extends AsyncTask<Void, Void, Void>{

        //static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/PriceWithoutPNR";
        static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/PriceWithoutPNRWithCache";

        // before background thread created
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hasGoodConnection = true;
            pDialog = new ProgressDialog(SearchResultRoundSecond.this,R.style.full_screen_dialog){
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.fill_dialog);
                    getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);

                    /*ImageView imageView = (ImageView) getWindow().findViewById(R.id.progressBar1);
                    Glide.with(SearchResultRoundSecond.this).load(R.raw.loading9).into(imageView);*/
                    timer = (TextView) getWindow().findViewById(R.id.timer);
                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                }
            };

            pDialog.setCancelable(false);
            pDialog.show();
        }

        // create background thread
        @Override
        protected Void doInBackground(Void... voids) {

            // construct URL from input parameter for url query
            Uri builtUrl = Uri.parse(REQUEST_URL).buildUpon()
                    .build();

            // create URL object from given string url
            URL url = createUrl(builtUrl.toString());
            Log.i("URL",builtUrl.toString());
            String jsonResponse = "";
            try {
                jsonResponse = makeHttpRequest(url);
                Log.i("JsonResponse Satu",jsonResponse);
            } catch (IOException e) {
                Log.e(TAG," Failed to create http request");
                hasGoodConnection = false;
                e.printStackTrace();
            }
            extractResponse(jsonResponse);
            return null;
        }

        // background thread finish
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            /*if (pDialog.isShowing())
                pDialog.dismiss();*/

            Log.d("TAG", "harga satu: " + totalHarga + " " + totalTax );
            if(hasNetworkConnection()){
                if(totalHarga != null)
                new SendHttpRequestSecond().execute();
            }else{
                // eg smartphone is in airplane mode
                Toast.makeText(SearchResultRoundSecond.this, "There is no network connection, activate your data cellular or connect through wifi connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // create URL object from url String
    public static URL createUrl(String urlStr){
        // return null if URL not created
        URL url = null;
        try {
            // create URL object
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            // create URL not allowed
            Log.e(TAG, "Problem building the URL ", e);
            e.printStackTrace();
        }
        return url;
    }

    // perform a network request
    public String makeHttpRequest(URL url) throws IOException {
        String jsonResponse="";

        if(url == null){
            Log.e(TAG,"There is no valid string url");
            hasGoodConnection = false;
            return null;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            // url.openConnection return HttpURLConnection that extend URLConnection
            urlConnection = (HttpURLConnection)url.openConnection();
            // http request method
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            // set timeout request
            urlConnection.setConnectTimeout(10000);/* milliseconds */
            urlConnection.setReadTimeout(10000);/* milliseconds */
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            // create jsonObject for HTTP request body
            JSONObject jsonRequestBody = createJsonObject();

            if(jsonRequestBody != null){
                // send the post body
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                bufferedWriter.write(jsonRequestBody.toString());
                bufferedWriter.flush();

                // log response message and response code
                Log.i("Response Code ", String.valueOf(urlConnection.getResponseCode()));
                Log.i("Response Message ", urlConnection.getResponseMessage());

                departureTimeList.clear();
                arrivalTimeList.clear();
                durationTimeList.clear();

            }else{
                Log.e(TAG, "There is no valid JSON object");
                hasGoodConnection = false;
            }

            // if the request was succesful (response code 200)
            // then read the input stream and parse the response
            if(urlConnection.getResponseCode() == 200){
                // get the input stream from given URL
                inputStream = urlConnection.getInputStream();
                // read inputStream and return a String
                jsonResponse = readFromInputStream(inputStream);
            }
            // else, there is no input stream contain information that we want to read
            else{
                Log.e(TAG,"Error response code: "+urlConnection.getResponseCode());
                hasGoodConnection = false;
            }
        }catch (Exception e){
            hasGoodConnection = false;
            // eg there is low cellular data on smartphone
            Log.e(TAG,"Problem retrieving JSON response.",e);
        }finally {
            // cleaning the resource

            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(inputStream != null){
                // function must handle java.io.IOException here
                inputStream.close();
            }
        }

        return jsonResponse;
    }

    // read input stream from API web services and return it to human readable format
    private static String readFromInputStream(InputStream inputStream) throws IOException {
        // to store information from input stream
        StringBuilder output = new StringBuilder();
        // read input stream
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        // represent input stream to human readable text format
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();
        while (line != null){
            output.append(line);
            line = bufferedReader.readLine();
        }
        // return information
        return  output.toString();
    }

    // create JSON object for HTTP POST request body
    public JSONObject createJsonObject(){

        try{
            JSONObject ptcGroupObject = new JSONObject();
            ptcGroupObject.put("value_qualifier", "ADT");
            ptcGroupObject.put("qualifier", "0");

            JSONArray ptcGroupArray = new JSONArray();
            ptcGroupArray.put(ptcGroupObject);

            JSONObject segmentControlDetailsObject = new JSONObject();
            segmentControlDetailsObject.put("number_of_units", 2);
            segmentControlDetailsObject.put("quantity", 1);

            JSONArray segmentControlDetailArray = new JSONArray();
            segmentControlDetailArray.put(segmentControlDetailsObject);

            JSONObject travellerDetailsObject = new JSONObject();
            travellerDetailsObject.put("measurement_value", 1);

            JSONObject travellerDetailsObjectDua = new JSONObject();
            travellerDetailsObjectDua.put("measurement_value", 2);

            JSONArray travellerDetailsArray = new JSONArray();
            travellerDetailsArray.put(travellerDetailsObject);
            travellerDetailsArray.put(travellerDetailsObjectDua);

            JSONObject passengerGroupObject = new JSONObject();
            passengerGroupObject.put("ptc_groups", ptcGroupArray);
            passengerGroupObject.put("segment_control_details", segmentControlDetailArray);
            passengerGroupObject.put("traveller_details", travellerDetailsArray);

            JSONArray passengerGroupArray = new JSONArray();
            passengerGroupArray.put(passengerGroupObject);

            JSONObject pricingOptionsGroup = new JSONObject();
            pricingOptionsGroup.put("marketing_company", "GA");

            JSONArray pricingOptionsGroupArray = new JSONArray();
            pricingOptionsGroupArray.put(pricingOptionsGroup);

            JSONArray flightIndicatorArray = new JSONArray();
            flightIndicatorArray.put("1");

            JSONObject segmentGroupObject = new JSONObject();
            segmentGroupObject.put("board_point_true_location_id", departureCode);
            segmentGroupObject.put("off_point_true_location_id", arrivalCode);
            segmentGroupObject.put("flight_marketing_company", "GA");
            segmentGroupObject.put("flight_number", "75");
            segmentGroupObject.put("booking_class", "Y");
            segmentGroupObject.put("flight_indicators", flightIndicatorArray);
            segmentGroupObject.put("item_number", 1);
            segmentGroupObject.put("departure_date", dateFormatApi);

            JSONArray segmentGroupArray = new JSONArray();
            segmentGroupArray.put(segmentGroupObject);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("message_function", "741");
            jsonObject.put("responsible_agency", "1A");
            jsonObject.put("passengers_groups", passengerGroupArray);
            jsonObject.put("pricing_options_groups", pricingOptionsGroupArray);
            jsonObject.put("destination", arrivalCode);
            jsonObject.put("origin", departureCode);
            jsonObject.put("segment_groups", segmentGroupArray);
            jsonObject.put("business_function", "1");


            Log.i("JSON object", jsonObject.toString());
            return jsonObject;
        }catch (JSONException e){
            Log.e(TAG,"Problem creating JSON object.",e);
            hasGoodConnection = false;
            return null;
        }
    }

    // parsing JSON response and initialize some variable from it
    public void extractResponse(String jsonResponse) {

        if(TextUtils.isEmpty(jsonResponse)){
            Log.e(TAG,"There is no valid json response");
            hasGoodConnection = false;
            return;
        }

        // If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // extract response data and initiate with variable that have been made
            JSONObject jsonObj = new JSONObject(jsonResponse);

            JSONArray pricingGroupLevelGroups = jsonObj.getJSONArray("pricingGroupLevelGroups");
            JSONObject pricingGroupLevelGroupsObject = pricingGroupLevelGroups.getJSONObject(0);

            totalHarga = pricingGroupLevelGroupsObject.getString("fareAmount");

            JSONArray taxDetailsArray = pricingGroupLevelGroupsObject.getJSONArray("taxDetails");

            for(int i = 0; i<taxDetailsArray.length(); i++){
                JSONObject taxDetailsObject = taxDetailsArray.getJSONObject(i);
                totalTax = totalTax + Integer.parseInt(taxDetailsObject.getString("taxDetailRate"));
            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(TAG, "Problem parsing JSON response", e);
            hasGoodConnection = false;
        }
    }

    // to check internet connection status on device eg smartphone
    boolean hasNetworkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED ) {
                // notify user you are online
                return true;
            }
            else if ( connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED
                    || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {
                // notify user you are not online
                return false;
            }
        }
        return false;
    }



    /**
     * Background thread second with all of error handling code start from here until the end of line
     * */

    @SuppressLint("StaticFieldLeak")
    private class SendHttpRequestSecond extends AsyncTask<Void, Void, Void>{

        //static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/PriceWithoutPNR";
        static final String REQUEST_URL = "https://garuda.client.nextflow.tech/garudatest/v1.0/PriceWithoutPNRWithCache";

        // before background thread created
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hasGoodConnection = true;
            /*pDialog = new ProgressDialog(SearchResultRoundSecond.this,R.style.full_screen_dialog){
                @Override
                protected void onCreate(Bundle savedInstanceState) {
                    super.onCreate(savedInstanceState);
                    setContentView(R.layout.fill_dialog);
                    getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT);

                    *//*ImageView imageView = (ImageView) getWindow().findViewById(R.id.progressBar1);
                    Glide.with(SearchResultRoundSecond.this).load(R.raw.loading9).into(imageView);*//*
                }
            };

            pDialog.setCancelable(false);
            pDialog.show();*/
        }

        // create background thread
        @Override
        protected Void doInBackground(Void... voids) {

            // construct URL from input parameter for url query
            Uri builtUrl = Uri.parse(REQUEST_URL).buildUpon()
                    .build();

            // create URL object from given string url
            URL url = createUrlSecond(builtUrl.toString());
            Log.i("URL",builtUrl.toString());
            String jsonResponse = "";
            try {
                jsonResponse = makeHttpRequestSecond(url);
                Log.i("JsonResponse",jsonResponse);
            } catch (IOException e) {
                Log.e(TAG," Failed to create http request");
                hasGoodConnection = false;
                e.printStackTrace();
            }
            extractResponseSecond(jsonResponse);
            return null;
        }

        // background thread finish
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

            if(hasGoodConnection){
                Log.d("TAG", "harga satu: " + totalHarga + " " + totalTax );
                Log.d("TAG", "harga dua: " + totalHargaDua + " " + totalTaxDua );
                Log.d("TAG", "info satu: " + totalPriceAwal + " " + timeFlightAwal + " " + flightIdentifierAwal + " " + durationFlightAwal );
                Log.d("TAG", "info dua: " + totalPrice + " " + timeFlight + " " + flightIdentifier + " " + durationFlight);

                intent = new Intent(SearchResultRoundSecond.this, FillDetailsRound.class);
                intent.putExtra("departureCode",departureCode);
                intent.putExtra("arrivalCode",arrivalCode);
                intent.putExtra("totalPax",totalPax);
                intent.putExtra("addedPrice",addedPrice);
                intent.putExtra("departureDate",departureDate);
                intent.putExtra("arrivalDate",arrivalDate);

                intent.putExtra("price",totalPriceAwal);
                intent.putExtra("timeFlight", timeFlightAwal);
                intent.putExtra("flightIdentification",flightIdentifierAwal);
                intent.putExtra("durationFlight",durationFlightAwal);

                intent.putExtra("priceDua",totalPrice);
                intent.putExtra("timeFlightDua", timeFlight);
                intent.putExtra("flightIdentificationDua",flightIdentifier);
                intent.putExtra("durationFlightDua",durationFlight);

                intent.putExtra("totalHarga",totalHarga);
                intent.putExtra("totalHargaDua",totalHargaDua);
                Log.i("Total Harga Dua ",totalHargaDua);
                intent.putExtra("totalTax",totalTax);
                intent.putExtra("totalTaxDua",totalTaxDua);
                intent.putExtra("totalTime",totalTime);

                intent.putExtra("departureStationTerminal",departureStationTerminal);
                intent.putExtra("arrivalStationTerminal",arrivalStationTerminal);
                startActivity(intent);
            }else{
                Toast.makeText(SearchResultRoundSecond.this, "Check your network connection and try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // create URL object from url String
    public static URL createUrlSecond(String urlStr){
        // return null if URL not created
        URL url = null;
        try {
            // create URL object
            url = new URL(urlStr);
        } catch (MalformedURLException e) {
            // create URL not allowed
            Log.e(TAG, "Problem building the URL ", e);
            e.printStackTrace();
        }
        return url;
    }

    // perform a network request
    public String makeHttpRequestSecond(URL url) throws IOException {
        String jsonResponse="";

        if(url == null){
            Log.e(TAG,"There is no valid string url");
            hasGoodConnection = false;
            return null;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            // url.openConnection return HttpURLConnection that extend URLConnection
            urlConnection = (HttpURLConnection)url.openConnection();
            // http request method
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            // set timeout request
            urlConnection.setConnectTimeout(10000);/* milliseconds */
            urlConnection.setReadTimeout(10000);/* milliseconds */
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);

            // create jsonObject for HTTP request body
            JSONObject jsonRequestBody = createJsonObjectSecond();

            if(jsonRequestBody != null){
                // send the post body
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
                bufferedWriter.write(jsonRequestBody.toString());
                bufferedWriter.flush();

                // log response message and response code
                Log.i("Response Code ", String.valueOf(urlConnection.getResponseCode()));
                Log.i("Response Message ", urlConnection.getResponseMessage());

                departureTimeList.clear();
                arrivalTimeList.clear();
                durationTimeList.clear();

            }else{
                Log.e(TAG, "There is no valid JSON object");
                hasGoodConnection = false;
            }

            // if the request was succesful (response code 200)
            // then read the input stream and parse the response
            if(urlConnection.getResponseCode() == 200){
                // get the input stream from given URL
                inputStream = urlConnection.getInputStream();
                // read inputStream and return a String
                jsonResponse = readFromInputStreamSecond(inputStream);
            }
            // else, there is no input stream contain information that we want to read
            else{
                Log.e(TAG,"Error response code: "+urlConnection.getResponseCode());
                hasGoodConnection = false;
            }
        }catch (Exception e){
            hasGoodConnection = false;
            // eg there is low cellular data on smartphone
            Log.e(TAG,"Problem retrieving JSON response.",e);
        }finally {
            // cleaning the resource

            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(inputStream != null){
                // function must handle java.io.IOException here
                inputStream.close();
            }
        }

        return jsonResponse;
    }

    // read input stream from API web services and return it to human readable format
    private static String readFromInputStreamSecond(InputStream inputStream) throws IOException {
        // to store information from input stream
        StringBuilder output = new StringBuilder();
        // read input stream
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        // represent input stream to human readable text format
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();
        while (line != null){
            output.append(line);
            line = bufferedReader.readLine();
        }
        // return information
        return  output.toString();
    }

    // create JSON object for HTTP POST request body
    public JSONObject createJsonObjectSecond(){

        try{
            JSONObject ptcGroupObject = new JSONObject();
            ptcGroupObject.put("value_qualifier", "ADT");
            ptcGroupObject.put("qualifier", "0");

            JSONArray ptcGroupArray = new JSONArray();
            ptcGroupArray.put(ptcGroupObject);

            JSONObject segmentControlDetailsObject = new JSONObject();
            segmentControlDetailsObject.put("number_of_units", 2);
            segmentControlDetailsObject.put("quantity", 1);

            JSONArray segmentControlDetailArray = new JSONArray();
            segmentControlDetailArray.put(segmentControlDetailsObject);

            JSONObject travellerDetailsObject = new JSONObject();
            travellerDetailsObject.put("measurement_value", 1);

            JSONObject travellerDetailsObjectDua = new JSONObject();
            travellerDetailsObjectDua.put("measurement_value", 2);

            JSONArray travellerDetailsArray = new JSONArray();
            travellerDetailsArray.put(travellerDetailsObject);
            travellerDetailsArray.put(travellerDetailsObjectDua);

            JSONObject passengerGroupObject = new JSONObject();
            passengerGroupObject.put("ptc_groups", ptcGroupArray);
            passengerGroupObject.put("segment_control_details", segmentControlDetailArray);
            passengerGroupObject.put("traveller_details", travellerDetailsArray);

            JSONArray passengerGroupArray = new JSONArray();
            passengerGroupArray.put(passengerGroupObject);

            JSONObject pricingOptionsGroup = new JSONObject();
            pricingOptionsGroup.put("marketing_company", "GA");

            JSONArray pricingOptionsGroupArray = new JSONArray();
            pricingOptionsGroupArray.put(pricingOptionsGroup);

            JSONArray flightIndicatorArray = new JSONArray();
            flightIndicatorArray.put("1");

            JSONObject segmentGroupObject = new JSONObject();
            segmentGroupObject.put("board_point_true_location_id", arrivalCode);
            segmentGroupObject.put("off_point_true_location_id", departureCode);
            segmentGroupObject.put("flight_marketing_company", "GA");
            segmentGroupObject.put("flight_number", "75");
            segmentGroupObject.put("booking_class", "Y");
            segmentGroupObject.put("flight_indicators", flightIndicatorArray);
            segmentGroupObject.put("item_number", 1);
            segmentGroupObject.put("departure_date", dateFormatApiDua);

            JSONArray segmentGroupArray = new JSONArray();
            segmentGroupArray.put(segmentGroupObject);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("message_function", "741");
            jsonObject.put("responsible_agency", "1A");
            jsonObject.put("passengers_groups", passengerGroupArray);
            jsonObject.put("pricing_options_groups", pricingOptionsGroupArray);
            jsonObject.put("destination", arrivalCode);
            jsonObject.put("origin", departureCode);
            jsonObject.put("segment_groups", segmentGroupArray);
            jsonObject.put("business_function", "1");

            Log.i("JSON object", jsonObject.toString());
            return jsonObject;
        }catch (JSONException e){
            Log.e(TAG,"Problem creating JSON object.",e);
            hasGoodConnection = false;
            return null;
        }
    }

    // parsing JSON response and initialize some variable from it
    public void extractResponseSecond(String jsonResponse) {

        if(TextUtils.isEmpty(jsonResponse)){
            Log.e(TAG,"There is no valid json response");
            hasGoodConnection = false;
            return;
        }

        // If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {
            // extract response data and initiate with variable that have been made
            JSONObject jsonObj = new JSONObject(jsonResponse);

            JSONArray pricingGroupLevelGroups = jsonObj.getJSONArray("pricingGroupLevelGroups");
            JSONObject pricingGroupLevelGroupsObject = pricingGroupLevelGroups.getJSONObject(0);

            totalHargaDua = pricingGroupLevelGroupsObject.getString("fareAmount");

            JSONArray taxDetailsArray = pricingGroupLevelGroupsObject.getJSONArray("taxDetails");

            for(int i = 0; i<taxDetailsArray.length(); i++){
                JSONObject taxDetailsObject = taxDetailsArray.getJSONObject(i);
                totalTaxDua = totalTaxDua + Integer.parseInt(taxDetailsObject.getString("taxDetailRate"));
            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e(TAG, "Problem parsing JSON response", e);
            hasGoodConnection = false;
        }
    }

    public Runnable runnable = new Runnable() {
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);

            totalTime = "" + String.format("%01d", Seconds) + "." + String.format("%03d", MilliSeconds);
            timer.setText(totalTime);

            handler.postDelayed(this, 0);
        }

    };
}
