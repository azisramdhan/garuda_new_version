package com.example.lotus.garudav3;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Lotus on 28/11/2017.
 */

public class DepartureDate extends AppCompatActivity {

    Toolbar toolbar;
    int selectedDate,selectedMonth,selectedYear;
    NestedScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_picker_full);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        CalendarView datePicker = (CalendarView) findViewById(R.id.date_picker);
        scrollView = (NestedScrollView) findViewById(R.id.scroll_view);

        scrollView.setNestedScrollingEnabled(false);
        scrollView.setOnTouchListener( new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        selectedDate=calendar.get(Calendar.DAY_OF_MONTH);
        selectedMonth=calendar.get(Calendar.MONTH);
        selectedYear=calendar.get(Calendar.YEAR);
        //datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

        datePicker.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                Log.e("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);

                Log.e("selected date", selectedDate+"");
                Log.e("selected date", dayOfMonth+"");
                Log.e("selected month", selectedMonth+"");
                Log.e("selected month", month+"");

                Log.e("selected year", selectedYear+"");
                Log.e("selected year", year+"");

                selectedDate=dayOfMonth;
                selectedMonth=(month);
                selectedYear=year;

                SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                Date date = new Date(selectedYear, selectedMonth, selectedDate-1);
                String dayOfWeek = simpledateformat.format(date);
                String monthStr = getMonth(month);
                String departureDateStr = "" + dayOfWeek + ", " + selectedDate + " " + monthStr;
                Intent intent = new Intent(DepartureDate.this, BookDetailActivity.class);
                intent.putExtra("departureDate", departureDateStr);
                startActivity(intent);
                finish();
                //Toast.makeText(DepartureDate.this, departureDateStr, Toast.LENGTH_SHORT).show();
            }
        });
/*
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {

            }

        });
*/
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }
}

